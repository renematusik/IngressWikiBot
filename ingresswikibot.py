# -*- coding: utf-8 -*-

import logging
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
import configparser

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

# set up a config file that contains the tg bot api key
config = configparser.ConfigParser()
config.read("config.ini")
tgapi = config["KEYS"]["tgapi"]

##### Commands #####

def start(bot, update):
    update.message.reply_text('Hello World!')

def home(bot, update):
    keyboard = [[InlineKeyboardButton("Items", callback_data="1"), InlineKeyboardButton("Actions", callback_data="2")],
                [InlineKeyboardButton("Game Elements", callback_data="3"), InlineKeyboardButton("Events", callback_data="4")],
                [InlineKeyboardButton("Lore", callback_data="5"), InlineKeyboardButton("Tools", callback_data="6")]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Choose a category:', reply_markup=reply_markup)

def items(bot, update):
    keyboard = [[InlineKeyboardButton("Shield", callback_data="1"), InlineKeyboardButton("Force Amp", callback_data="2"), InlineKeyboardButton("Turret", callback_data="3")],
                [InlineKeyboardButton("Multi Hacks", callback_data="4"), InlineKeyboardButton("Heat Sink", callback_data="5"), InlineKeyboardButton("Power Cube", callback_data="6")],
                [InlineKeyboardButton("XMP Burster", callback_data="7"), InlineKeyboardButton("Ultra Strike", callback_data="8"), InlineKeyboardButton("Capsule", callback_data="9")],
                [InlineKeyboardButton("ADA / Jarvis", callback_data="10"), InlineKeyboardButton("Portal Key", callback_data="11"), InlineKeyboardButton("Media", callback_data="12")]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Choose an Item:', reply_markup=reply_markup)

def actions(bot, update):
    keyboard = [[InlineKeyboardButton("Deploy", callback_data="1"), InlineKeyboardButton("Upgrade", callback_data="2"), InlineKeyboardButton("Recharge", callback_data="3")],
                [InlineKeyboardButton("Linking", callback_data="4"), InlineKeyboardButton("Fielding", callback_data="5"), InlineKeyboardButton("Attacking", callback_data="6")],
                [InlineKeyboardButton("Hacking", callback_data="7"), InlineKeyboardButton("Glyph Hacking", callback_data="8"), InlineKeyboardButton("Drop", callback_data="9")],
                [InlineKeyboardButton("Modding", callback_data="10"), InlineKeyboardButton("Recycle", callback_data="11"), InlineKeyboardButton("Level Up", callback_data="12")]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Choose a Action:', reply_markup=reply_markup)

def links(bot, update):
    update.message.reply_text("Links are used to connect Portals and make fields")

def button(bot, update):
    query = update.callback_query

    bot.edit_message_text(text="Selected option: %s" % query.data,
                          chat_id=query.message.chat_id,
                          message_id=query.message.message_id)

def error(bot, update, error):
    logging.warning('Update "%s" caused error "%s"' % (update, error))

# Create the Updater and pass it your bot's token.
updater = Updater(tgapi)

updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CommandHandler("home", home))
updater.dispatcher.add_handler(CommandHandler("links", links))
updater.dispatcher.add_handler(CommandHandler("items", items))
updater.dispatcher.add_handler(CommandHandler("actions", actions))
updater.dispatcher.add_handler(CallbackQueryHandler(button))
updater.dispatcher.add_error_handler(error)

# Start the Bot
updater.start_polling()

# Run the bot until the user presses Ctrl-C or the process receives SIGINT,
# SIGTERM or SIGABRT
updater.idle()